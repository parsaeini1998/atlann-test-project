import psycopg2
import mysql.connector
import csv
import os


class DataBase(object):
    def __init__(
            self, p_db, p_user, p_password, p_host, m_db,
            m_password, m_user, m_host):
        self.postgre_db_conn = psycopg2.connect(
            database=p_db,
            user=p_user,
            password=p_password,
            host=p_host,
            port='5432'
        )
        print(self.postgre_db_conn)
        print("Successfully connected to PostgreSQL Database -> source")
        self.postgre_db_conn.autocommit = True

        self.mysql_db_conn = mysql.connector.connect(
            host=m_host,
            user=m_user,
            password=m_password,
            port="32769",
            database=m_db
        )
        print(self.mysql_db_conn)
        print("Successfully connected to MySQL Database -> destination")
        self.initialization_commands = [
            """ CREATE TABLE person (
                    id INTEGER PRIMARY KEY,
                    name VARCHAR(32) NOT NULL ,
                    last_name VARCHAR(32) NOT NULL
        ) 
        """,
            """
             CREATE TABLE book (
                    id INTEGER PRIMARY KEY,
                    title VARCHAR(64) NOT NULL ,
                    author_name VARCHAR(64) NOT NULL
            ) 
            """
        ]

    def postgre_sql_command(self, command, params=None, close_cursor=True):
        try:
            cursor = self.postgre_db_conn.cursor()
            cursor.execute(command, params)
            if close_cursor:
                cursor.close()
            return cursor
        except (Exception, psycopg2.DatabaseError) as error:
            print(error)

    def mysql_sql_command(self, command, params=None):
        try:
            cursor = self.mysql_db_conn.cursor()
            cursor.execute(command, params)
            self.mysql_db_conn.commit()
        except Exception as error:
            print(error)

    def source_db_initialization(self, relative_file_path):
        for command in self.initialization_commands:
            self.postgre_sql_command(command)

        base_path = os.path.abspath(os.path.dirname(__file__))
        file_path = os.path.join(base_path, relative_file_path)
        with open(file_path, 'r') as file:
            reader = csv.reader(file)
            for row in reader:
                print(row)
                if row[0] == "P":
                    command = """
                    INSERT INTO person (id, name, last_name)
                    VALUES (%s, %s, %s);
                    """
                    params = (int(row[1]), row[2], row[3])
                    self.postgre_sql_command(command, params)
                elif row[0] == "B":
                    command = """
                    INSERT INTO book (id, title, author_name)
                    VALUES (%s, %s, %s);
                    """
                    params = (int(row[1]), row[2], row[3])
                    self.postgre_sql_command(command, params)

    def destination_db_initialization(self):
        for command in self.initialization_commands:
            self.mysql_sql_command(command)

    def source_db_fetch(self, table):
        command = """
        SELECT * FROM %s
        """ % table

        cursor = self.postgre_sql_command(command, close_cursor=False)

        if cursor is None:
            print("input data isin incorrect,probably the table name!")
            return None
        records = cursor.fetchall()
        print("records", records)
        return records

    def destination_db_insert(self, table, records):
        if records is None:
            print("Fetched data had some problems;Check input data file")
            return
        for record in records:
            command_table = "INSERT INTO %s " % table
            full_command = command_table + """
            VALUES (%s, %s, %s)
            """
            params = record
            self.mysql_sql_command(full_command, params)

    def databases_initialization(self, relative_file_path):
        self.source_db_initialization(relative_file_path)
        self.destination_db_initialization()

    def transfer(self, table):
        records = self.source_db_fetch(table)
        self.destination_db_insert(table, records)
