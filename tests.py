import json
import unittest

from services.database import DataBase


class DatabaseTestCase(unittest.TestCase):

    def setUp(self) -> None:
        file = open('data/test_project_input.json')
        input_data = json.load(file)
        source_db = input_data["databases"][0]["A"]
        destination_db = input_data["databases"][0]["B"]
        self.database = DataBase(
            p_host=source_db.get("connection-ip"),
            p_password=source_db.get("password"),
            p_user=source_db.get("username"),
            p_db=source_db.get("db-name"),
            m_host=destination_db.get("connection-ip"),
            m_password=destination_db.get("password"),
            m_user=destination_db.get("username"),
            m_db=destination_db.get("db-name")
        )
        self.database.databases_initialization("../data/test_initial_data.csv")

        self.database.transfer(source_db.get("table-name"))

    def test_source_initialization(self):
        command = "SELECT * FROM person"
        cursor = self.database.postgre_sql_command(command, close_cursor=False)
        records = cursor.fetchall()
        expected_data = [
            (1, 'Parsa', 'Eini'),
            (2, 'Mohamad', 'Hasani')
        ]
        self.assertTupleEqual(records[0], expected_data[0])
        self.assertTupleEqual(records[1], expected_data[1])


if __name__ == '__main__':
    unittest.main()
