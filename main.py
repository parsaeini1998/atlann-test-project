import json

from services.database import DataBase
from services.mail import EmailService

if __name__ == '__main__':
    # get the input data from json file
    file = open('data/project_input.json')
    input_data = json.load(file)
    source_db = input_data["databases"][0]["A"]
    destination_db = input_data["databases"][0]["B"]
    contact_info = input_data["contact-info"]["contact-value"]

    # connect to databases and initialize them
    database = DataBase(
        p_host=source_db.get("connection-ip"),
        p_password=source_db.get("password"),
        p_user=source_db.get("username"),
        p_db=source_db.get("db-name"),
        m_host=destination_db.get("connection-ip"),
        m_password=destination_db.get("password"),
        m_user=destination_db.get("username"),
        m_db=destination_db.get("db-name")
    )
    database.databases_initialization("../data/initial_data.csv")

    # transfer data
    database.transfer(source_db.get("table-name"))

    # send an email to the email provided
    receivers = [contact_info]
    message = "Some Text"
    EmailService.send_email(
        message, receivers, "atlannp@gmail.com", "12345678atlann"
    )
